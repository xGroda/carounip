# Título: 
Carounip, um app para auxiliar o transporte universitário.

## Autores:
Allan, Groda ...

## Resumo: 
Tem como objetivo ajudar universitários com sua locomoção fornecendo uma maneira alternativa e segura para chegar à universidade. Os únicos usuários do app serão os próprios alunos da Unip.

## ‐ OBJETIVOS GERAIS: 
Conseguir ajudar alunos que possuem dificuldades com os meios de transporte, ou que moram a grandes distâncias da universidade a chegarem de maneira rápida e segura com outros alunos dispostos a dar carona.

## ‐ OBJETIVOS ESPECÍFICOS: (metas)

## Público alvo: 
Estudantes Universitários.

## Recursos:

## Método:
O aplicativo conterá(de maneira resumida) uma tela de Login, para acesso serão necessários RA e senha, ou seja o usuário tem que ser aluno da Unip. 

Dentro do app uma tela com o Perfil contendo algumas informações, como Nome, RA, foto(talvez, para aumentar nível de segurança), poderá conter também informações do carro como Placa, Cor e Modelo, para os alunos que oferecerem carona. 

Uma tela para a busca por carona, com campos de região, valor, saido de..., data e um botão Procurar(Buscar, Go).

Tela com o resultado da pesquisa contendo foto, nome, valor, região de saida, bairros, com opções de ajuste de valor(talvez).

Clicando na carona que é mostrada no resultado o usuário sera direcionado a uma página com todas as informações do caronista.